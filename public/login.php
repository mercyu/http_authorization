<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');
function getToken() {
  $secret = 'abcdefghijklmnopqrstuvwxyz1234567890';
  $n = strlen($secret);
  $new = '';
  for ($i = 0; $i < $n; $i++) {
      $new .= substr($secret, mt_rand(1, $n) - 1, 1);
  }
  return $new;
}

// устанавливаем продолжительность существования сессии
ini_set('session.gc_maxlifetime', time() + 60 * 60);
// Начинаем сессию.
session_start();
$_SESSION['csrf_token'] = getToken();
// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login'])) {
  // Если есть логин в сессии, то пользователь уже авторизован.
  // Сделать выход (окончание сессии вызовом session_destroy()
  //при нажатии на кнопку Выход).
  // Делаем перенаправление на форму.
  header('Location: ./');
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  ?>
  <html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width; initial-scale=1">
      <link rel="stylesheet" href="styles.css">
  </head>
  <body>
  <form class="transparent" action="" method="post">
  <input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token'] ?>">
  <div class="form">
    <label>
    <p>Login: </p>
     <input name="login" />
    </label>
    <label>
    <p>Password: </p>
     <input name="pass" />
     </label>
     <label>
    <input id="log" type="submit" value="Sign in" />
    </label>
    <div id="replacehtml"></div>
    </div>
  </form>
   </body>
    </html>
  <?php
  }
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {
  // Проверяем, есть ли такой логин и пароль в базе данных.
  // Выдаем сообщение об ошибках.
  $user = 'u20378';
  $pass = '3861391';  
  $db = new PDO('mysql:host=localhost;dbname=u20378', $user, $pass,
      array(PDO::ATTR_PERSISTENT => true));
  // Подготовленный запрос. Не именованные метки.
  try {
      $stmt = $db->prepare("SELECT password FROM application WHERE login = ?"); //Подготавливает SQL-запрос к базе данных к запуску посредством метода PDOStatement::execute().
      $stmt->execute(htmlspecialchars($_POST['login']));
      $result = $stmt->fetch(PDO::FETCH_ASSOC); //возвращает массив, индексированный именами столбцов результирующего набора
      if($result==NULL){
        //$messages1['not'] = '<div class="nonexist">This user has not been found</div>';
      die("This user has not been found");
      }
      else if (md5(htmlspecialchars($_POST['pass'])) !== $result['password'])
      // $messages1['notpass'] = '<div class="nonexist">Invalid password</div>';
       die("Invalid password");
       else {
           $_SESSION['login'] = htmlspecialchars($_POST['login']);
           $_SESSION['pass'] = htmlspecialchars($_POST['pass']);
              //$messages1['exist'] = '';
               }
        }
  catch(PDOException $e){
      print('Error : failed to connect to database' . $e->getMessage());
      exit();
  }
/*?>  <script>
          $(document).ready(function(){
              $("#log").click(function(){
                  $("#replacehtml").html("<div class="authorization" ><?php if (!empty($messages1)) { foreach ($messages1 as $message) {
                		    print($message);}} ?></div>");
              });
          });
              </script>
          <?php 
          */

  // Делаем перенаправление.
  header('Location: ./');
}
