<?php
//session_destroy();
ini_set('session.gc_maxlifetime', time() + 60 * 60); // время жизни сессии
session_start();

//session_unset() очищает переменную $_SESSION. Это эквивалентно $_SESSION = array();
// это влияет только на локальный экземпляр переменной $_SESSION, но не на данные сеанса в хранилище сеансов.

//В отличие от этого, session_destroy() уничтожает данные сеанса, хранящиеся в хранилище сеансов (например, файл сеанса в файловой системе).
//Все остальное остается неизменным.

unset($_SESSION['login']);
unset($_SESSION['pass']);

unset($_COOKIE['save']);
unset($_COOKIE[session_name()]);

header('Location: index.php');