CREATE TABLE application (
id int(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
login varchar(10) NOT NULL,
password varchar(40) NOT NULL,
fio varchar(128) NOT NULL DEFAULT '',
email varchar(30) NOT NULL DEFAULT '', 
date varchar(20) NOT NULL DEFAULT '', 
sex varchar(10) NOT NULL DEFAULT '', 
education varchar(10) NOT NULL DEFAULT '', 
Java int NOT NULL, 
Python int NOT NULL, 
PHP int NOT NULL, 
C int NOT NULL, 
comment varchar(128) NOT NULL DEFAULT '' 
);

CREATE TABLE adminData (
login varchar(32) NOT NULL,
password varchar(32) NOT NULL
);
