<?php
if(empty($path['form'])) die('Access denied');   // защита от include
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width; initial-scale=1">
    <link rel="stylesheet" href="styles.css">
  </head>
  <body>
    <div class="cover-block"> 
<form class="transparent" action="" method="POST">
<div class="form">
<h3>Application for the course</h3>

<div class="sign" ><?php if (!empty($_COOKIE['pass'])) {print($messages['sign']);} ?></div>
<div class="logout">
 <button onclick="document.location.replace('logout.php');">sign out</button>
</div>

<label>
       <p>Fio: </p> 
   <input name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>" placeholder="Sam Smith">
   <?php if (!empty($messages['fio'])) {print($messages['fio']);} ?>
   </label>
  <label>
       <p>Email: </p>
   <input name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" type="email" placeholder="test@example.com"> 
   <?php if (!empty($messages['email'])) {print($messages['email']);} ?>
      </label>
  <label>
      <p>Date of birth: </p>
  <input name="date" <?php if ($errors['date']) {print 'class="error"';} ?> value="<?php print $values['date']; ?>" type="date" min="1920-01-01" max="2005-01-01">
  <?php if (!empty($messages['date'])) {print($messages['date']);} ?>
      </label>
     <label>
      <p>Sex:<br>
      <label class="radio"><input name="sex" <?php if ($errors['sex']) {print 'class="error"';} ?> checked="<?php if($values['sex'] == 'woman'){print 'checked';} ?>" type="radio" value="woman"> woman</label> 
      <label class="radio"><input name="sex" <?php if ($errors['sex']) {print 'class="error"';} ?> checked="<?php if($values['sex'] == 'man'){print 'checked';} ?>" type="radio" value="man"> man</label></br></p>
      <?php if (!empty($messages['sex'])) {print($messages['sex']);} ?>
 </label> 
 <label>
      <p>Education: <br>
	  <label class="radio"><input name="edu" type="radio" value="1" checked="<?php if($values['edu'] == '1') {print 'checked';} ?>"> Basic</label>
      <label class="radio"><input name="edu" type="radio" value="2" checked="<?php if($values['edu'] == '2') {print 'checked';} ?>"> Secondary general</label>
      <label class="radio"><input name="edu" type="radio" value="3" checked="<?php if($values['edu'] == '3') {print 'checked';} ?>"> Secondary special</label>
      <label class="radio"><input name="edu" type="radio" value="4" checked="<?php if($values['edu'] == '4') {print 'checked';} ?>"> Incomplete higher</label>
      <label class="radio"><input name="edu" type="radio" value="5" checked="<?php if($values['edu'] == '5') {print 'checked';} ?>"> Higher</label><br></p>
      <?php if (!empty($messages['edu'])) {print($messages['edu']);} ?>
 </label>
 <label>
      <p>Courses:</p>
  <select name="course[]" multiple="multiple" <?php if ($errors['check']) {print 'class="error"';} ?>>
 <option value="Java" <?php if(in_array('Java',unserialize($values['course']))) {print 'selected';} ?> >Java</option>
 <option value="Python" <?php if(in_array('Python',unserialize($values['course']))) {print 'selected';} ?>>Python</option>
 <option value="PHP" <?php if(in_array('PHP',unserialize($values['course']))) {print 'selected';} ?>>PHP</option>
 <option value="C" <?php if(in_array('C',unserialize($values['course']))) {print 'selected';} ?>>C</option>
  </select>
</label>
<?php if (!empty($messages['course'])) {print($messages['course']);} ?>
   <label>
      <p>Comment:</p>
		<textarea name="comment" <?php if ($errors['comment']) {print 'class="error"';} ?> rows="3" cols="40"><?php print $values['comment']; ?></textarea>
          </label>
          <?php if (!empty($messages['comment'])) {print($messages['comment']);} ?>
  <label>
      <p>Consent to the processing of personal data:</p>
		<input name="check" <?php if ($errors['check']) {print 'class="error"';} ?> checked="<?php if($values['check']) print 'checked'; ?>" type="checkbox" id="check"> I agree
        </label>
        <?php if (!empty($messages['check'])) {print($messages['check']);} ?>
  
  <p> <input type="submit" value="SEND" /></p>
  <p> <button class="exit" hidden="<?php if (empty($_SESSION['login'])) {print('true');} else {print('false');} ?>" onclick="session_destroy()"></button></p>
  </div>
</form>
</div>
</body>
</html>
