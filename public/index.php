<?php
//  Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');
$path = array();
$path['form'] = 'form.php'; // защита от include
// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Массив для временного хранения сообщений пользователю.
  $messages = array();
  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
      // Если есть параметр save, то выводим сообщение пользователю.
      ?><script> alert('Thank you, the results are saved.');</script>
      <?php
   // Если в куках есть пароль, то выводим сообщение с ссылкой на страницу входа.
   if (!empty($_COOKIE['pass'])) {
    $messages['sign'] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
      и паролем <strong>%s</strong> для изменения данных.',
      strip_tags($_COOKIE['login']),
      strip_tags($_COOKIE['pass']));
  }
}

// Складываем признак ошибок в массив.
$errors = array();
$errors['fio'] = !empty($_COOKIE['fio_error']);
$errors['email'] = !empty($_COOKIE['email_error']);
$errors['date'] = !empty($_COOKIE['date_error']);
$errors['sex'] = !empty($_COOKIE['sex_error']);
$errors['edu'] = !empty($_COOKIE['edu_error']);
$errors['course'] = !empty($_COOKIE['course_error']);
$errors['comment'] = !empty($_COOKIE['comment_error']);
$errors['check'] = !empty($_COOKIE['check_error']);

// Выдаем сообщения об ошибках.
if ($errors['fio']) {
  // Удаляем куку, указывая время устаревания в прошлом.
  setcookie('fio_error', '', 100000);
  // Выводим сообщение.
 if ($errors['fio'] == 'empty') $messages['fio'] = '<div class="errors">FIO is required.</div>';
 else $messages['fio'] = '<div class="errors">Only letters and white space allowed.</div>';
}
if ($errors['email']) {
    setcookie('email_error', '', 100000);
   if($errors['email'] == 'empty') $messages['email'] = '<div class="errors">Email is required.</div>';
   else $messages['email'] = '<div class="errors">Invalid email format.</div>';
  }
if ($errors['date']) {
    setcookie('date_error', '', 100000);
   $messages['date'] = '<div class="errors">Date is required.</div>';
  }
if ($errors['sex']) {
    setcookie('sex_error', '', 100000);
   $messages['sex'] = '<div class="errors">Sex is required.</div>';
  }
if ($errors['edu']) {
    setcookie('edu_error', '', 100000);
   $messages['edu'] = '<div class="errors">Education is required.</div>';
  }
if ($errors['course']) {
    setcookie('course_error', '', 100000);
   $messages['course'] = '<div class="errors">Course is required.</div>';
  }
if ($errors['comment']) {
    setcookie('comment_error', '', 100000);
   $messages['comment'] = '<div class="errors">Comment is required.</div>';
  }
if ($errors['check']) {
    setcookie('check_error', '', 100000);
   $messages['check'] = '<div class="errors">Consent is required.</div>';
  }

// Складываем предыдущие значения полей в массив, если есть.
 // При этом санитизуем все данные для безопасного отображения в браузере.
$values = array();
$values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
$values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
$values['date'] = empty($_COOKIE['date_value']) ? '' : strip_tags($_COOKIE['date_value']);
$values['sex'] = empty($_COOKIE['sex_value']) ? '' : strip_tags($_COOKIE['sex_value']);
$values['edu'] = empty($_COOKIE['edu_value']) ? '' : strip_tags($_COOKIE['edu_value']);
$values['course'] = empty($_COOKIE['course_value']) ? '' : strip_tags($_COOKIE['course_value']);
$values['comment'] = empty($_COOKIE['comment_value']) ? '' : strip_tags($_COOKIE['comment_value']);
$values['check'] = empty($_COOKIE['check_value']) ? '' : strip_tags($_COOKIE['check_value']);

// Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
  // ранее в сессию записан факт успешного логина.
  if (empty($errors) && !empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
    // загружаем данные пользователя из БД
    // и заполняем переменную $values,
    // предварительно санитизовав.
    $user = 'u20378';
    $pass = '3861391';
    $db = new PDO('mysql:host=localhost;dbname=u20378', $user, $pass,
        array(PDO::ATTR_PERSISTENT => true));
   
    try {
      // Подготовленный запрос. Не именованные метки.
            $stmt = $db->prepare("SELECT Java FROM application WHERE login = ?"); //Подготавливает SQL-запрос к базе данных к запуску посредством метода PDOStatement::execute().
            $stmt->execute([$_SESSION['login']]);
            $course1 = strip_tags($stmt->fetchColumn()) == 1 ? 'Java ' : ''; //fetchColumn() возвращает данные одного столбца следующей строки результирующего набора
                                                                                 //fetch(PDO::FETCH_ASSOC) ассоциативный массив не заработал
            $stmt = $db->prepare("SELECT Python FROM application WHERE login = ?");
            $stmt->execute([$_SESSION['login']]);
            $course2 = strip_tags( $stmt->fetchColumn()) == 1 ? 'Python ' : '';
            
            $stmt = $db->prepare("SELECT PHP FROM application WHERE login = ?");
            $stmt->execute([$_SESSION['login']]);
            $course3 = strip_tags($stmt->fetchColumn()) == 1 ? 'PHP ' : '';
            
            $stmt = $db->prepare("SELECT C FROM application WHERE login = ?");
            $stmt->execute([$_SESSION['login']]);
            $course4 = strip_tags($stmt->fetchColumn()) == 1 ? 'C' : '';
            
            $values['course'] = serialize(explode(' ',$course1. $course2. $course3. $course4)); //конкатенация строк и превращение в массив
            
            $stmt = $db->prepare("SELECT fio FROM application WHERE login = ?");
            $stmt->execute([$_SESSION['login']]);
            $values['fio'] = strip_tags($stmt->fetchColumn());
            
            $stmt = $db->prepare("SELECT email FROM application WHERE login = ?");
            $stmt->execute([$_SESSION['login']]);
            $values['email'] = strip_tags($stmt->fetchColumn());
            
            $stmt = $db->prepare("SELECT date FROM application WHERE login = ?");
            $stmt->execute([$_SESSION['login']]);
            $values['date'] = strip_tags( $stmt->fetchColumn());
            
            $stmt = $db->prepare("SELECT sex FROM application WHERE login = ?");
            $stmt->execute([$_SESSION['login']]);
            $values['sex'] = strip_tags($stmt->fetchColumn());
            
            $stmt = $db->prepare("SELECT education FROM application WHERE login = ?");
            $stmt->execute([$_SESSION['login']]);
            $values['edu'] = strip_tags( $stmt->fetchColumn());
            
            $stmt = $db->prepare("SELECT comment FROM application WHERE login = ?");
            $stmt->execute([$_SESSION['login']]);
            $values['comment'] = strip_tags($stmt->fetchColumn());
            
            $values['check'] = true;
         
           // die(printf('Entered with login %s, uid %d', $_SESSION['login'], $_SESSION['uid']));
    }
    catch(PDOException $e){
        die('Error : failed to connect to database' . $e->getMessage());
        exit();
        }        
  }
  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else{
$errors = FALSE;

if (empty($_POST['fio'])) {
    // Выдаем куку до конца сессии с флажком об ошибке в поле fio.
    setcookie('fio_error', 'empty');
  $errors = TRUE;
}
else { if (!preg_match('/^[A-Za-zа-яА-Я\s]+$/iu',$_POST['fio'])) {
    setcookie('fio_error', 'invalid');
    $errors = TRUE;
       }
       else {
    // Сохраняем ранее введенное в форму значение на год.
    setcookie('fio_value', $_POST['fio'], time() + 365 * 24 * 60 * 60);
       }
    }

if (empty($_POST['email'])) {
    setcookie('email_error', 'empty');
    $errors = TRUE;
}
else { if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
    setcookie('fio_error', 'invalid');
    $errors = TRUE;
       }
       else {
    setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
       }
     }

if (empty($_POST['date'])) {
    setcookie('date_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('date_value', $_POST['date'], time() + 365 * 24 * 60 * 60);
  }


if (!$_POST['sex']){
    setcookie('sex_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('sex_value', $_POST['sex'], time() + 365 * 24 * 60 * 60);
  }


if (!$_POST['edu']){
    setcookie('edu_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('edu_value', $_POST['edu'], time() + 365 * 24 * 60 * 60);
  }


if (empty($_POST['course'])) {
    setcookie('course_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('course_value', serialize($_POST['course']), time() + 365 * 24 * 60 * 60);
  }


if (!$_POST['comment']){
    setcookie('comment_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('comment_value', $_POST['comment'], time() + 365 * 24 * 60 * 60);
  }


if (!$_POST['check']){
    setcookie('check_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('check_value', $_POST['check'], time() + 365 * 24 * 60 * 60);
  }



if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('date_error', '', 100000);
    setcookie('sex_error', '', 100000);
    setcookie('edu_error', '', 100000);
    setcookie('course_error', '', 100000);
    setcookie('comment_error', '', 100000);
    setcookie('check_error', '', 100000);
  }

  $user = 'u20378';
  $pass = '3861391';
  $db = new PDO('mysql:host=localhost;dbname=u20378', $user, $pass,
      array(PDO::ATTR_PERSISTENT => true));
  

   // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
   if (!empty($_COOKIE[session_name()]) &&
   session_start() && !empty($_SESSION['login'])&& 
   $_POST['csrf_token'] == $_SESSION['csrf_token'] ) {
 //перезаписываем данные в БД новыми данными,
 // кроме логина и пароля.
 $ability1 = in_array('Java', $_POST['course']) ? 1 : 0;
 $ability2 = in_array('Python', $_POST['course']) ? 1 : 0;
 $ability3 = in_array('PHP', $_POST['course']) ? 1 : 0;
 $ability4 = in_array('C', $_POST['course']) ? 1 : 0;

 try {$stmt = $db->prepare("UPDATE application SET fio = ?, email = ?, date = ?, sex = ?, education = ?, Java = ?, Python = ?, PHP = ?, C = ?, comment = ? WHERE login = $_SESSION['login']");
  $stmt->execute(array($_POST['fio'],htmlspecialchars($_POST['email']),$_POST['date'],$_POST['sex'],$_POST['edu'],$ability1,$ability2,$ability3,$ability4,htmlspecialchars($_POST['comment']),$_SESSION['login']));
  }
  catch(PDOException $e){
      print('Error : failed to connect to database' . $e->getMessage());
      exit();
  }

}
else {
 // Генерируем уникальный логин и пароль.
 function login_generation() {
  $nlogin = "";
  $new_login = str(mt_rand(100,999)); 
  $new_login .= chr(mt_rand(97, 122));
  return $new_login;
 }

 function pass_generation($length) {
  $npassword = "";
  for ($i = 0; $i < $length; $i++){
     $case = mt_rand(0, 2);
     if($case == 0)
     $new_password .= chr(mt_rand(48, 57));// 48 - это 0, а 57 - 9
     else {
       if($case == 1)
        $new_password .= chr(mt_rand(65, 90)); // 65 - это A, а 90 - Z
       else
        $new_password .= chr(mt_rand(97, 122)); // 97 - это a, а 122 - z
     }
    }
  return $new_password;
 }

 $login = login_generation();
 $password = pass_generation(5);
 // Сохраняем в Cookies.
 setcookie('login', $login);
 setcookie('pass', $password);

 $_POST['login'] = $login;
 $_POST['pass'] = $password;
  
 //Сохранение данных формы, логина и хеш md5() пароля в базу данных.
  
 $ability1 = in_array('Java', $_POST['course']) ? 1 : 0;
 $ability2 = in_array('Python', $_POST['course']) ? 1 : 0;
 $ability3 = in_array('PHP', $_POST['course']) ? 1 : 0;
 $ability4 = in_array('C', $_POST['course']) ? 1 : 0;

  //Подготовленный запрос
  //неименованные метки
  
  try {$stmt = $db->prepare("INSERT INTO application SET login = ?, password = ?, fio = ?, email = ?, date = ?, sex = ?, education = ?, Java = ?, Python = ?, PHP = ?, C = ?, comment = ?");
    $stmt->execute(array($login,md5($password),$_POST['fio'],htmlspecialchars($_POST['email']),$_POST['date'],$_POST['sex'],$_POST['edu'],$ability1,$ability2,$ability3,$ability4,htmlspecialchars($_POST['comment'])));
  }
  catch(PDOException $e){
      print('Error : failed to connect to database' . $e->getMessage());
      exit();
  }
}
  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: ./');
}
